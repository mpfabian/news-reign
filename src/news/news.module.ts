import { Module } from '@nestjs/common';
import { NewsController } from './news.controller';
import { NewsService } from './news.service';
import { MongooseModule } from '@nestjs/mongoose';
import { News } from './schemas/news.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'News', schema: News }
    ])
  ],
  controllers: [NewsController],
  providers: [NewsService],
})
export class NewsModule {}
