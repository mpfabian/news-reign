/* export class CreateNewsDTO {
  readonly name: string;
  readonly description: string; */


export class CreateNewsDTO {
  created_at:       Date;
  title:            null | string;
  url:              null | string;
  author:           string;
  points:           number | null;
  story_text:       null;
  comment_text:     null | string;
  num_comments:     number | null;
  story_id:         number | null;
  story_title:      null | string;
  story_url:        null | string;
  parent_id:        number | null;
  created_at_i:     number;
  _tags:            string[];
  objectID:         string;
  _highlightResult: HighlightResult;
}

export class HighlightResult {
  author: Author;
  comment_text: CommentText;
  story_title: StoryTitle;
  story_url: StoryUrl;
}

export class Author {
  value: string;
  matchLevel: string;
  matchedWords: any[];
}

export class CommentText {
  value: string;
  matchLevel: string;
  fullyHighlighted: boolean;
  matchedWords: string[];
}

export class StoryTitle {
  value: string;
  matchLevel: string;
  matchedWords: any[];
}

export class StoryUrl {
  value: string;
  matchLevel: string;
  matchedWords: any[];
}