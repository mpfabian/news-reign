import { Injectable, NotFoundException } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { News } from './interfaces/news.interface';
import { CreateNewsDTO } from './dto/news.dto';

@Injectable()
export class NewsService {
  constructor(@InjectModel('News') private readonly newsModel: Model<News>) {}

  async createNews(createNewsDTO: CreateNewsDTO): Promise<News> {
    const news = new this.newsModel(createNewsDTO);
   
    return await news.save();
  }

  async getAllNews(): Promise<News[]> {
    const newsList = await this.newsModel.find();
    return newsList;
  }

  async getNewsById(id: string): Promise<News> {
    try{
      return await this.newsModel.findById(id);
    }catch(e){
      throw new NotFoundException(`News ID: "${id}" does not exists`);
    }
  }

  async deleteNews(id: string): Promise<News> {
    try{
      const newsDeleted = await this.newsModel.findByIdAndDelete(id);
      if(!newsDeleted) { throw new NotFoundException(`News ID: "${id}" does not exists`); }
      return newsDeleted;
    }catch(error){
      throw new NotFoundException(`News ID: "${id}" does not exists`);
    }
    
  }

}
