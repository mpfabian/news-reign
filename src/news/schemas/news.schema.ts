import { Schema } from 'mongoose';

/* export const NewsSchema = new Schema({
    created_at: Date,
    title: {}, 
    description: String,
    author: String,
    url: {},
    points: {},
    story_texT: {},
    comment_text: {},
    num_comments: {},
    story_id: Number,
    story_title: String,
    story_url: String,
    parent_id: Number,
    created_at_i: Number,
    _tags: {},
    objectID: {},
    _highlightResult: {}
}); */

export const News = new Schema( {
    "created_at": {
      "type": "Date"
    },
    "title": {
      "type": "Mixed"
    },
    "url": {
      "type": "Mixed"
    },
    "author": {
      "type": "String"
    },
    "points": {
      "type": "Mixed"
    },
    "story_text": {
      "type": "Mixed"
    },
    "comment_text": {
      "type": "String"
    },
    "num_comments": {
      "type": "Mixed"
    },
    "story_id": {
      "type": "Number"
    },
    "story_title": {
      "type": "String"
    },
    "story_url": {
      "type": "String"
    },
    "parent_id": {
      "type": "Number"
    },
    "created_at_i": {
      "type": "Number"
    },
    "_tags": {
      "type": [
        "String"
      ]
    },
    "objectID": {
      "type": "String"
    },
    "_highlightResult": {
      "author": {
        "value": {
          "type": "String"
        },
        "matchLevel": {
          "type": "String"
        },
        "matchedWords": {
          "type": "Array"
        }
      },
      "comment_text": {
        "value": {
          "type": "String"
        },
        "matchLevel": {
          "type": "String"
        },
        "fullyHighlighted": {
          "type": "Boolean"
        },
        "matchedWords": {
          "type": [
            "String"
          ]
        }
      },
      "story_title": {
        "value": {
          "type": "String"
        },
        "matchLevel": {
          "type": "String"
        },
        "matchedWords": {
          "type": "Array"
        }
      },
      "story_url": {
        "value": {
          "type": "String"
        },
        "matchLevel": {
          "type": "String"
        },
        "matchedWords": {
          "type": "Array"
        }
      }
    }
  }
)