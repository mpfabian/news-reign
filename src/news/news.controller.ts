import {
  Controller,
  Post,
  Get,
  Delete,
  Query,
  NotFoundException,
  Param,
  Res,
  HttpStatus,
  Body,
} from '@nestjs/common';
import { CreateNewsDTO } from './dto/news.dto';
import { News } from './interfaces/news.interface';
import { NewsService } from './news.service';

@Controller('news')
export class NewsController {
  constructor(private newsService: NewsService) {}

  @Post('/create')
  async createNews(@Res() Res, @Body() createNewsDTO: CreateNewsDTO) {
    const news = await this.newsService.createNews(createNewsDTO);
    return Res.status(HttpStatus.OK).json({
      message: 'News Successfully Created',
      news
    });
  } 

  @Get('/getAllNews')
  async getAllNews(){
    return await this.newsService.getAllNews();
  }

  @Get('/:id')
  async getNewsById(@Param('id') id: string): Promise<News>{
    return await this.newsService.getNewsById(id);
  }

  @Delete('/delete/:id')
  async deleteNews(@Param('id') id: string): Promise<News>{
    return await this.newsService.deleteNews(id);
  }

}
