import { Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import axios from "axios";
import { CreateNewsDTO } from './news/dto/news.dto';

@Injectable()
export class AppService {
  private readonly logger = new Logger(AppService.name);
  //constructor(private newsService: NewsService) {}

  

  @Cron(CronExpression.EVERY_HOUR, {
    name: 'oncePerHour',
  })
  everyHour() {
    this.logger.debug('Called PER HOUR');
  }

  @Cron('*/5 * * * * *', {
    name: 'oncePerSecond',
  })
  async every5Second() {
    this.logger.debug('Called when the current second is 5');

    const response = await axios.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')

    const arrayNews = response.data.hits;
    for (let news of arrayNews) {
      console.log(news);
      //llamar servicio create of news
    }

  }

}